import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-twitter',
	templateUrl: './twitter.component.html',
	styleUrls: ['./twitter.component.scss']
})
export class TwitterComponent implements OnInit {

	twitterHandle: string;

 	constructor() {
 		this.twitterHandle = "";
 	}

 	ngOnInit() {
 	}

}