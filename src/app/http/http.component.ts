import { Component, OnInit } from '@angular/core';
import { HttpClient } from  "@angular/common/http";

@Component({
	selector: 'app-http',
	templateUrl: './http.component.html',
	styleUrls: ['./http.component.scss']
})
export class HttpComponent implements OnInit {

	currencyTo: string;
	currencyFrom: string;
	currencies: string[];
	amount: number;
	baseUrl: string = "https://api.exchangeratesapi.io/";
	conversion: number;

	constructor(private httpClient: HttpClient) {
		this.currencyTo = "";
		this.currencyFrom = "";
		this.currencies = [];
		this.amount = 0;
		this.getCurrencies();
	}

	ngOnInit() {
	}

	getCurrencies() {
		this.httpClient.get(this.baseUrl + 'latest').subscribe((res)=>{
            for (var key in res.rates) {
            	this.currencies.push(key);
            }
            this.currencies.sort((a, b) => (a > b ? 1 : -1));
            this.setCurrencyFrom(this.currencies[0]);
            this.setCurrencyTo(this.currencies[0]);
        });
	}

	setCurrencyFrom(currencyFrom: string) {
		this.currencyFrom = currencyFrom;
		this.clearConversion();
	}

	setCurrencyTo(currencyTo: string) {
		this.currencyTo = currencyTo;
		this.clearConversion();
	}

	getConversionRate() {
        this.httpClient.get(this.baseUrl + 
        	'latest?base=' + this.currencyFrom + 
        	"&symbols=" + this.currencyTo)
        .subscribe((res)=>{
        	for (var key in res.rates) {
            	this.conversion = (res.rates[key] * this.amount);
            }
        });
	}

	clearConversion() {
		this.conversion = 0;
	}
}
