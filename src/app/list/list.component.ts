import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-list',
	templateUrl: './list.component.html',
	styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

	newListItem: string;
	itemList: string[];

	constructor() {
		this.newListItem = "";
		this.itemList = [];
	}

	ngOnInit() {
	}

	addListItem() {
		if (this.newListItem.length == 0) {
			console.log("warning, adding empty list item");
		}
		this.itemList.push(this.newListItem);
	}

	removeListItem() {
		if (this.itemList.length > 0) {
			this.itemList.pop();
		} else {
			console.log("warning, attempting to pop nothing, doing nothing...");
		}
	}

}
