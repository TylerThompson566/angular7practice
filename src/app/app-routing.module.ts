import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { HelloComponent } from './hello/hello.component';
import { TwitterComponent } from './twitter/twitter.component';
import { ListComponent } from './list/list.component';
import { HttpComponent } from './http/http.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'hello', component: HelloComponent },
  { path: 'twitter', component: TwitterComponent },
  { path: 'list', component: ListComponent },
  { path: 'http', component: HttpComponent },
];

@NgModule({
 	imports: [RouterModule.forRoot(routes)],
 	exports: [RouterModule]
})
export class AppRoutingModule { }
